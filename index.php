<?php
    require "database.php";
    //require "token.php";
    require "notif.php";

    require 'vendor/autoload.php';
    use \Firebase\JWT\JWT;

    $iss='http://soda-armatislc.com';
    // This is the secret key of the api secret
    $key = 'CWCc9h7JnMZcRCXMR9MpaGjkT6Qfb5gB';

    $lockFunctions = ['jwtDecode','jwtEncode'];

    //************************************//
    //   fonction de traçabilité  //
    //************************************//
    /** 
    *   
    *   
    *   params  : 
    *       $function    : nom de la méthode appelé sur l'API
    *       $args    : Array comprenants les arguments passé à la méthode
    *       $userID  : Id de l'utilisateur ayant réalisé la requête
    *   fonction:
    *   Permet d'enregistrer les actions réalisé par les utilisateurs sur l'API
    */

    function trace(string $function, array $args, int $userID = NULL){
        $argstoEncrypt=["reponse_profil","password","reponse"];
        foreach($argstoEncrypt as $label){
            if(isset($args[$label])){
                $args[$label]=password_hash($args[$label], PASSWORD_DEFAULT);
            }
        }
        $pdo = init_pdo();
        $query="INSERT INTO trace (userId_trace, function_trace, args_trace) VALUES(:userId, :function, :args)";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':function', $function);
        $prep->bindValue(':args', json_encode ($args));
        $prep->bindValue(':userId', $userID);
        $prep->execute();
    }

    function jwtDecode(string $jwt){
        global $key;
        // if jwt is not empty
        if($jwt){
            // if decode succeed, show user details
            try {
                // decode jwt
                $decoded = JWT::decode($jwt, base64_decode(strtr($key, '-_', '+/')), ['HS512']);
                
                return (array)$decoded;
                
            }
            catch (Exception $e){
            
                // set response code
                http_response_code(401);
            
                // show error message
                echo json_encode(array(
                    "message" => "Access denied.",
                    "error" => $e->getMessage()
                ));
                die();
            }
        }// show error message if jwt is empty
        else{
        
            // set response code
            http_response_code(401);
        
            // tell the user access denied
            echo json_encode(array("message" => "Access denied."));
            die();
        }
    }

    function jwtEncodeFromUserData(array $userData){
        global $iss, $key;
        $issuedAt   = time();          
        $expire     = $issuedAt+ 216000; 

        $token_payload = [
            'iss' => $iss,
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => base64_encode(random_bytes(32)),          // Json Token Id: an unique identifier for the token
            'nbf'  => $issuedAt,        // Not before
            'exp'  => $expire,           // Expire 
            'sub' => $userData['id_profil'],
            'name' => $userData['prenom_profil'].' '.$userData['nom_profil'],
            'email' => $userData['mail']
        ];

        $jwt = JWT::encode($token_payload, base64_decode(strtr($key, '-_', '+/')), 'HS512');
        return $jwt;
    }

    function jwtEncodeFromPayload(array $token_payload){
        global $key;
        $issuedAt   = time();          
        $expire     = $issuedAt+ 216000; 

        $token_payload['iat'] = $issuedAt;        // Issued at: time when the token was generated
        $token_payload['jti']  = base64_encode(random_bytes(32));          // Json Token Id: an unique identifier for the token
        $token_payload['nbf']  = $issuedAt;       // Not before
        $token_payload['exp']  = $expire;         // Expire 

        $jwt = JWT::encode($token_payload, base64_decode(strtr($key, '-_', '+/')), 'HS512');
        return $jwt;
    }
    /** 
    *   Requête d'inscription
    *   
    *   Methode :    POST
    *   params  : 
    *       $action : id de l'action a effectuer
    *       $site   : id du site geographique
    *       $mail   : id de connexion
    *       $token  : token du device client
    *       $nom    : nom de l'utiliateur
    *       $prenom : prenom de l'utilisateur
    *   fonction:
    *   Verification de la présence ou non de l'uti
    *   -lisateur en base de donnée pour eviter les
    *   doublons.
    *   Insertion en base de donnée de l'utilisateur 
    *   a sont inscription.
    *   Envoie d'une reponse de statut au client.
    */ 
    function SignUp(){
        $pdo = init_pdo();
        $site=$_POST['site'];
        $mail=$_POST['mail'];
        $token=$_POST['token'];
        if(/*isset($_POST['token']) &&*/ isset($_POST['mail']))
        {
          $query="SELECT * FROM profil WHERE mail_profil = '".$mail."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $data=$prep->fetchAll(PDO::FETCH_ASSOC);
            if(Count($data)==0)
            {
                $query="INSERT INTO profil (nom_profil, prenom_profil, fonction_profil, mail_profil, password_profil, token_profil, photo_profil, id_site, reponse_profil) VALUES(:nom, :prenom, :fonction, :mail, :password, :token, :photo, :site, :reponse)";
                $prep=$pdo->prepare($query)or die('Erreur SQL !');
                $prep->bindValue(':nom', $_POST['nom']);
                $prep->bindValue(':prenom', $_POST['prenom']);
                $prep->bindValue(':fonction', $_POST['fonction']);
                $prep->bindValue(':mail', $_POST['mail']);
                $prep->bindValue(':password', password_hash($_POST['password'], PASSWORD_DEFAULT));
                $prep->bindValue(':token', $_POST['token']);
                $prep->bindValue(':photo', "default.jpg");
                $prep->bindValue(':site', $_POST['site']);
                $prep->bindValue(':reponse', password_hash($_POST['reponse_profil'], PASSWORD_DEFAULT));
                $prep->execute();
                $var = array();
                $var["res"]  = "true";
                $var["msg"]  = "Utilisateur inscrit";
                echo json_encode($var);
              }
            else
            {
                    $var = array();
                    $var["res"]  = "false";
                    $var["msg"]  = "Utilisateur déja existant";
                    echo json_encode($var);
            }
        }
    }
        
    /** 
    *   Requête de modification de profil
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $id         : id du profil
    *       $nom        : nom de l'utiliateur
    *       $prenom     : prenom de l'utilisateur
    *       $fonction   : fonction de l'utilisateur
    *       $site       : nom du site geographique
    *       $path       : chemin accées photo profil
    *   fonction:
    *   Modification données du profil utilisateur 
    *   avec modification ou non de la photo profil.
    *   Envoie d'une reponse de statut au client.
    */ 
    function UpdateProfil(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $id = $jwtData['sub'];
        $Nom = $_POST['nom'];
        $Prenom = $_POST['prenom'];
        $Fonction = $_POST['fonction'];
        $site=$_POST['site'];
        $mail=$_POST['mail'];
        $oldPassword=$_POST['oldPassword'];
        $newPassword=$_POST['newPassword'];
                $reponse=$_POST['reponse'];
        $var = array();
        $var["res"]  = "true";
        $var["msg"]  = "Utilisateur modifier";
        
        if(isset($_POST['photo']))
        {
            $path = "./img_Profil/".$id.".jpg";
            if(file_exists($path))
            {
                unlink($path) or die("Couldn't delete file");
            }
            file_put_contents($path,base64_decode($_POST['photo']));
            $query="UPDATE profil SET photo_profil='".substr($path, 13)."' WHERE id_profil='".$id."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !');
            $prep->execute();
        }
    
        if(isset($_POST['oldPassword']) && isset($_POST['newPassword']) && !empty($_POST['oldPassword']) && !empty($_POST['newPassword'])){
            $query="SELECT password_profil FROM profil WHERE id_profil = '".$id."'";
            $prep=$pdo->prepare($query);
            $prep->execute();
            $data=$prep->fetchAll(PDO::FETCH_ASSOC);
            if(Count($data)==0)
            {
                $var = array();
                $var["res"] = "false";
                $var["msg"]  = "Requete SQL vide";
            }
            else
            {
                if(password_verify($oldPassword, $data[0]['password_profil']) == false )
                {
                    $var = array();
                    $var["res"] = "false";
                    $var["msg"]  = "Mauvais mot de passe";
                }    
                else
                {
                    $hashedPassword = password_hash($_POST['newPassword'], PASSWORD_DEFAULT);
                    $query="UPDATE profil SET password_profil='".$hashedPassword."' WHERE id_profil='".$id."'";
                    $prep=$pdo->prepare($query);
                    $prep->execute();       
                } 
            }
        }
        
        if(isset($_POST['mail']) && !empty($_POST['mail'])){
            $query="UPDATE profil SET mail_profil='".$mail."' WHERE id_profil='".$id."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !');
            $prep->execute();    
        }
        
        if(isset($_POST['nom']) && !empty($_POST['nom'])){
            $query="UPDATE profil SET nom_profil='".$Nom."' WHERE id_profil='".$id."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !');
            $prep->execute();   
        }
        
        if(isset($_POST['prenom']) && !empty($_POST['prenom'])){
            $query="UPDATE profil SET prenom_profil='".$Prenom."' WHERE id_profil='".$id."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !');
            $prep->execute();   
        }
        
        if(isset($_POST['fonction']) && !empty($_POST['fonction'])){
            $query="UPDATE profil SET fonction_profil='".$Fonction."' WHERE id_profil='".$id."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !');
            $prep->execute();   
        }
        
        if(isset($_POST['site']) && !empty($_POST['site'])){
            $query="UPDATE profil SET id_site='".$site."' WHERE id_profil='".$id."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !');
            $prep->execute();   
        }
        
        if(isset($_POST['reponse']) && !empty($_POST['reponse'])){
            $hashedReponse = password_hash($_POST['reponse'], PASSWORD_DEFAULT);
            $query="UPDATE profil SET reponse_profil='".$hashedReponse."' WHERE id_profil='".$id."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !');
            $prep->execute(); 
        }
        
        
        // else {
        //     $var = array();
        //     $var["res"] = "false";
        //     $var["msg"]  = "L'ID de l'utilisateur n'est pas specifie";
        // }
    
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);    
    }

    /** 
    *   Requête de connexion
    *   
    *   Methode :    POST
    *   params  : 
    *       $action : id de l'action a effectuer
    *       $mail   : id de connexion
    *       $Mdp    : password de connexion
    *       $token  : token du device client
    *   fonction:
    *   Recuperation des identifiant de connexion 
    *   en base de données.
    *   Verificaton avec les données envoyer par 
    *   le client.
    *   Décodage et verification du mots de passe
    *   Envoie d'une reponse de satut au client
    */
    function SignIn(){
        
        
        if(empty($_POST['mail'])||empty($_POST['password']))
        {
            // set response code
            http_response_code(401);
            // show error message
            echo json_encode(array(
                "res" => "false",
                "msg" => "Veuiller renseigner l'email et le mot de passe"
            ));
            die();
        }
        $pdo = init_pdo();
        $Email=$_POST['mail'];
        $Mdp=$_POST['password'];
        

        $query="SELECT P.id_profil, P.nom_profil, P.prenom_profil, P.fonction_profil, P.mail_profil, P.password_profil, P.photo_profil, P.id_site, S.libelle_site,S.gps_site FROM profil P INNER JOIN site S On P.id_site=S.id_site WHERE mail_profil = '".$Email."'";
        $prep=$pdo->prepare($query);
        $prep->execute();
        $data=$prep->fetchAll(PDO::FETCH_ASSOC);
        if(Count($data)==0)
        {
            $var = array();;
            // set response code
            http_response_code(401);
            $var["res"] = "false";
            $var["msg"]  = "Mauvais identifiants";
            echo "[".json_encode($var)."]";
        }
        else
        {
            if(password_verify($Mdp, $data[0]['password_profil']) == false )
            {
                $var = array();
                // set response code
                http_response_code(401);
                $var["res"] = "false";
                $var["msg"]  = "Mot de passe incorrect";
                echo "[".json_encode($var)."]";
                die();
            }
            if(isset($_POST['token']))
            {
                $token=$_POST['token'];
                $query="UPDATE profil SET token_profil='".$token."' WHERE id_profil='".$data[0]['id_profil']."'";
                $prep=$pdo->prepare($query);
                $prep->execute();       
            }
            // This is the id token
            $jwt = jwtEncodeFromUserData($data[0]);

            echo json_encode($data+['jwt'=>$jwt]);
        }
    }         
            
    //************************************//
    //          fonction profil           //
    //************************************//
    /** 
    *   Requête de supression des données 
    *   utilisateur
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $id         : id du profil
    *       $image_doc  : chemin d'accées dossier photo
    *   fonction:
    *   Suppression du profil utilisateur.
    *   Supression de toute les donnée se rapportant
    *   a l'utilisateur supprimmer (events, commentaire..)
    *   Envoie d'une reponse de statut au client.
    */
    function suppProfil(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $id=$jwtData['sub'];
        $query="SELECT id_reunion FROM reunion WHERE id_profil= '".$id."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $idR=$prep->execute();
        $query="DELETE FROM profil WHERE id_profil= '".$id."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $image_doc = "./img_Events/";
        $ouverture = opendir ($image_doc); 
        while (false !== ($fichier = readdir($ouverture))) 
        {
            $chemin = $image_doc."/".$fichier;
            //si le fichier n'est pas un répertoire
            if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier)) 
            {
                $nom = explode(".", $fichier);
                    if($nom[0]== $idR)
                    {
                        unlink($chemin); 
                    } 

                }
        }
        closedir($ouverture);
        
        $image_doc2 = "./img_Profil/";
        $ouverture2 = opendir ($image_doc2); 
        $lecture2 = readdir($ouverture2);
        while (false !== ($fichier2 = readdir($ouverture2))) 
        {
            $chemin2 = $image_doc2."/".$fichier2;
            //si le fichier n'est pas un répertoire
            if ($fichier2 != ".." AND $fichier2 != "." AND !is_dir($fichier2)) 
            {
                $nom = explode(".", $fichier2);
                if($nom[0]== $jwtData['sub'])
                {
                    unlink($chemin2); 
                } 
            }
        }
        closedir($ouverture2);
        $var = array();
        $var["res"]  = "true";
        $var["msg"]  = "Profil supprimé";
        echo json_encode($var);
    }
        
            
    /** 
    *   Requête d'affichage donnée profil
    *   
    *   Methode :    POST
    *   params  : 
    *       $action : id de l'action a effectuer
    *       $id   : id de profil
    *   fonction:
    *   Recuperation des donnée d'un profil.
    *   Retour d'une liste de donnée format JSON
    *   Envoie d'une reponse de statut au client.
    */ 
    function Afficher(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $id = $jwtData['sub'];
        $query="SELECT P.id_profil, P.nom_profil, P.prenom_profil, P.fonction_profil, P.photo_profil, P.id_site, S.libelle_site,S.gps_site FROM profil P INNER JOIN site S On P.id_site=S.id_site WHERE id_profil= '".$id."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $Element=$prep->fetchAll(PDO::FETCH_ASSOC);
        $jwt = jwtEncodeFromPayload($jwtData);
        
        echo json_encode(['profil'=>$Element]+['jwt'=>$jwt]);
    }
            
        
            
    /** 
     *   Requête d'affichage events de l'utilisateur
     *   
     *   Methode :    POST
     *   params  : 
     *       $action     : id de l'action a effectuer
     *       $element    : tableau d'events
     *       $id         : id de l'utiliateur
     *   fonction:
     *   Envoie des données d'event d'un profil 
     *   utilisateur. 
     *   Envoie d'une reponse de statut au client.
     */ 
    function AfficherEventProfil(){ 
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);  
        $Element= array();
        $id = $jwtData['sub'];
        $query="SELECT * FROM reunion WHERE id_profil='".$id."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $Element=$prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT R.id_reunion, R.objet_reunion, R.heure_reunion, R.date_reunion, R.adresse_reunion, R.lieu_reunion, R.nbPlace_reunion, R.duree_reunion, R.id_profil, R.photo_reunion FROM reunion R INNER JOIN participant P ON R.id_reunion=P.id_reunion WHERE P.id_profil='".$id."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $Element+=$prep->fetchAll(PDO::FETCH_ASSOC);
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode(['eventProfil'=>$Element]+['jwt'=>$jwt]);
    }
            
        
            
    /** 
     *   Requête d'affichage des profil utilisateur
     *   
     *   Methode :    POST
     *   params  : 
     *       $action     : id de l'action a effectuer
     *   fonction:
     *   Envoie de tout les profil de la BDD 
     *   Envoie d'une reponse de statut au client.
     */
    function AfficherAll(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $query="SELECT  id_profil, nom_profil, prenom_profil, fonction_profil, photo_profil, id_site FROM profil WHERE id_profil !=0 ";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $Element['profils']=$prep->fetchAll(PDO::FETCH_ASSOC);
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($Element+['jwt'=>$jwt]);
    }
        
            
    //************************************//
    //          fonction Events           //
    //************************************//
    /** 
    *   Requête d'affichage des events
    *   
    *   Methode :    GET
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idS        : id de site
    *       $idP        : id de l'utiliateur
    *   fonction:
    *   Envoie des données d'event  
    *   Envoie d'une reponse de statut au client.
    */
    function listEvent(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);

        $query="SELECT DISTINCT id_site FROM profil WHERE id_profil='".$jwtData['sub']."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $idS=$prep->fetchAll(PDO::FETCH_ASSOC);
        $idS=$idS[0]['id_site'];


        $element=array();
        $query=" SELECT R.id_reunion, R.objet_reunion, R.heure_reunion, R.date_reunion, R.adresse_reunion, R.lieu_reunion, R.nbPlace_reunion, R.duree_reunion, R.photo_reunion, R.id_profil, R.type_reunion FROM reunion R LEFT JOIN inviter I ON R.id_reunion=I.id_reunion LEFT JOIN participant PA ON R.id_reunion=PA.id_reunion LEFT JOIN profil P ON R.id_profil=P.id_profil WHERE I.id_profil='".$jwtData['sub']."' OR PA.id_profil='".$jwtData['sub']."' OR ( P.id_site='".$idS."' AND R.type_reunion='Publique') OR R.id_profil='".$jwtData['sub']."' GROUP BY R.id_reunion ORDER BY date_reunion, heure_reunion ASC ";
        
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element['Event']=$prep->fetchAll(PDO::FETCH_ASSOC);


        $query="SELECT COUNT(*) FROM notification WHERE id_profil='".$jwtData['sub']."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $count=$prep->fetchAll(PDO::FETCH_ASSOC);
        if($count>0)
        {
            $element['notif']=true;
        }
        else
        {
            $element['notif']=false;
        }
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($element+['jwt'=>$jwt]);
    }
            
        
            
    /** 
    *   Requête de création d'events
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $objet      : titre de l'events
    *       $heure      : horaire de l'events
    *       $date       : date de l'events
    *       $adresse    : adresse de l'events
    *       $lieu       : lieu de l'events
    *       $nbPlace    : nombre de places de l'events
    *       $duree      : durée de l'events
    *       $id         : id de l'organisateur
    *       $photo      : photo de l'events 
    *   fonction:
    *   Insertion d'un events dans la base de donnée avec
    *   tout ses invités.
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function createEvent(){
        
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
            $query="INSERT INTO reunion (objet_reunion, heure_reunion, date_reunion, adresse_reunion, lieu_reunion, nbPlace_reunion, duree_reunion, photo_reunion,id_profil, type_reunion) VALUES(:objet, :heure, :date, :adresse, :lieu, :nbPlace, :duree, :photo, :id, :typeReunion)";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':objet', $_POST['objet']);
        $prep->bindValue(':heure', $_POST['heure']);
        $prep->bindValue(':date', Date("Y-m-d", strtotime($_POST['date'])));
        $prep->bindValue(':adresse', $_POST['adresse']);
        $prep->bindValue(':lieu', $_POST['lieu']);
        $prep->bindValue(':nbPlace', $_POST['nbPlace']);
        $prep->bindValue(':duree', $_POST['duree']);
        $prep->bindValue(':photo', NULL);
        $prep->bindValue(':id', $jwtData['sub']);
        $prep->bindValue(':typeReunion', $_POST['typeReunion']);
        
        $prep->execute();
        $idR=$pdo->lastInsertId();
        
        if(isset($_POST['photo']))
        {
            $query="UPDATE reunion SET photo_reunion='".$idR.".jpg' WHERE id_reunion ='".$idR."'";
            $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $path = "./img_Events/".$idR.".jpg";
            file_put_contents($path,base64_decode($_POST['photo']));
        }
        $invite=json_decode($_POST['listInviter']); 
                
        foreach($invite as $v)
        {
            $query="INSERT INTO inviter (id_reunion, id_profil) VALUES(:idR, :idP)";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->bindValue(':idR', $idR);
            $prep->bindValue(':idP', $v);
            $prep->execute(); 
            $query="INSERT INTO notification (contenu_notif, date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, :idR, :idP)";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->bindValue(':contenu',"Vous avez reçu une invitation à '".$_POST['objet']."'");
            $prep->bindValue(':date', $_POST['currentDate']);
            $prep->bindValue(':statu', "0");
            $prep->bindValue(':type', "0");
            $prep->bindValue(':idR', $idR);
            $prep->bindValue(':idP', $v);
            $prep->execute(); 
            $query="SELECT token_profil FROM profil WHERE id_profil='".$v."'";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $data=$prep->fetchAll(PDO::FETCH_ASSOC);
            if(count($data)!=0)
            {
                notification($data[0]['token_profil'], "Invitation", "Vous avez reçu une invitation à '".$_POST['objet']."'");
            }   
        };
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = $idR;
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }
            
        
            
    /** 
    *   Requête de modification d'events
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $objet      : titre de l'events
    *       $heure      : horaire de l'events
    *       $date       : date de l'events
    *       $adresse    : adresse de l'events
    *       $lieu       : lieu de l'events
    *       $nbPlace    : nombre de places de l'events
    *       $duree      : durée de l'events
    *       $id         : id de l'organisateur
    *       $photo      : photo de l'events 
    *   fonction:
    *   Modification d'un events dans la base de donnée avec
    *   Modification de la photo ou non.
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function UpdateEvent(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $objet = $_POST['objet'];
        $heure = $_POST['heure'];
        $date = Date("Y-m-d", strtotime($_POST['date']));
        $adresse = $_POST['adresse'];
        $lieu = $_POST['lieu'];
        $place = $_POST['nbPlace'];
        $duree = $_POST['duree'];
        $idR = $_POST['id'];
        $typeReunion = $_POST['typeReunion'];

        if(isset($_POST['photo']))
        {
            $path = "./img_Events/".$idR.".jpg";
            unlink($path); 
            file_put_contents($path,base64_decode($_POST['photo']));
            $query="UPDATE reunion SET objet_reunion ='".$objet."', heure_reunion ='".$heure."', date_reunion ='".$date."', adresse_reunion ='".$adresse."', lieu_reunion ='".$lieu."', nbPlace_reunion ='".$place."', duree_reunion ='".$duree."', photo_reunion ='".$idR.".jpg', type_reunion ='".$typeReunion."' WHERE id_reunion='".$idR."' AND id_profil='".$jwtData['sub']."'";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $var = array();
            $var["res"]  = "True";
            $var["msg"]  = "Event modifier avec photo";
            $jwt = jwtEncodeFromPayload($jwtData);
            echo json_encode($var+['jwt'=>$jwt]);  
        }
        elseif(!isset($_POST['photo'])) 
        {
            $query="UPDATE reunion SET objet_reunion ='".$objet."', heure_reunion ='".$heure."', date_reunion ='".$date."', adresse_reunion ='".$adresse."', lieu_reunion ='".$lieu."', nbPlace_reunion ='".$place."', duree_reunion ='".$duree."', type_reunion ='".$typeReunion."' WHERE id_reunion='".$idR."'  AND id_profil='".$jwtData['sub']."'";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $var = array();
            $var["res"]  = "True";
            $var["msg"]  = "Event modifier";
            $jwt = jwtEncodeFromPayload($jwtData);
            echo json_encode($var+['jwt'=>$jwt]);  
        }
        $query="SELECT id_profil FROM participant WHERE id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $participant=$prep->fetchAll(PDO::FETCH_ASSOC);
        foreach($participant as $v)
        {
            $query="INSERT INTO notification (contenu_notif, date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, :idR, :idP)";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->bindValue(':contenu', "La réunion '".$_POST['objet']."' a été modifiée");
            $prep->bindValue(':date', $_POST['currentDate']);
            $prep->bindValue(':statu', "0");
            $prep->bindValue(':type', "2");
            $prep->bindValue(':idR', $jwtData['sub']);
            $prep->bindValue(':idP', $v["id_profil"]);
            $prep->execute();
            $query="SELECT token_profil FROM profil WHERE id_profil='".$v["id_profil"]."'";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $data=$prep->fetchAll(PDO::FETCH_ASSOC);         
            if(count($data)!=0)
            {
                $objet = $_POST['objet'];
                notification($data[0]['token_profil'], "Réunion modifiée", "La réunion '".$_POST['objet']."'a été modifiée");
            }
        }
    }
            
        
            
    /** 
    *   Requête d'affichage des details d'events
    *   
    *   Methode :    GET
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $element    : tableau de donnée d'events
    *       $id         : id de l'events
    *   fonction:
    *   Récupération de l'ensemble des donnée qui compose
    *   un events.
    *   Envoie des données au client format JSON.  
    *   Envoie d'une reponse de statut au client.
    */
    function detailEvent(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idR= intval($_GET['id']);
        $element=array();
        $query="SELECT R.id_reunion, R.objet_reunion, R.heure_reunion, R.date_reunion, R.adresse_reunion, R.lieu_reunion, R.nbPlace_reunion, R.duree_reunion, R.photo_reunion, R.type_reunion FROM reunion R WHERE id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["reunion"]=$prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT P.id_profil, P.nom_profil, P.prenom_profil, P.fonction_profil, P.photo_profil FROM profil P INNER JOIN reunion R ON P.id_profil=R.id_profil WHERE id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["organisateur"] = $prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT P.id_profil, P.nom_profil, P.prenom_profil, P.fonction_profil, P.photo_profil, S.libelle_site FROM profil P INNER JOIN participant PA ON P.id_profil=PA.id_profil INNER JOIN reunion R ON R.id_reunion=PA.id_reunion INNER JOIN site S ON P.id_site=S.id_site WHERE R.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["participants"] = $prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT P.id_profil, P.nom_profil, P.prenom_profil, P.fonction_profil, P.photo_profil, S.libelle_site FROM profil P INNER JOIN inviter I ON P.id_profil=I.id_profil INNER JOIN reunion R ON R.id_reunion=I.id_reunion INNER JOIN site S ON P.id_site=S.id_site  WHERE R.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["inviter"] = $prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT C.id_commentaire, C.contenu_commentaire, C.date_commentaire, C.heure_commentaire, P.id_profil, P.nom_profil, P.prenom_profil FROM commentaire C INNER JOIN posseder PO ON C.id_commentaire=PO.id_commentaire INNER JOIN profil P ON C.id_profil=P.id_profil where PO.id_reunion='".$idR."'ORDER BY date_commentaire, heure_commentaire ASC";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["commentaires"] = $prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT N.id_note, N.valeur_note, N.id_profil, N.id_reunion, P.nom_profil, P.prenom_profil FROM note N INNER JOIN profil P ON N.id_profil=P.id_profil WHERE N.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["notes"] = $prep->fetchAll(PDO::FETCH_ASSOC);
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode(['reunion'=>$element,'jwt'=>$jwt]);
    }
        
            
    /** 
    *   Requête de supression d'events
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idR        : id de l'event
    *       $image_doc  : chemein d'accées dossier photo
    *   fonction:
    *   Supression d'un event dans la base de donnée avec
    *   toute les données se rapportant a cette event.
    *   Insertion d'une notifications au profils concerné.
    *   Envoie d'une notification push au profils concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function suppEvent(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt); 
        $idR = $_POST['idR'];
        $query="SELECT id_profil FROM participant WHERE id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $participant=$prep->fetchAll(PDO::FETCH_ASSOC);

        $query="DELETE C FROM commentaire C INNER JOIN posseder P ON P.id_commentaire=C.id_commentaire WHERE P.id_reunion IN (SELECT PO.id_reunion FROM posseder PO WHERE PO.id_reunion= '".$idR."')"; 
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $query="DELETE FROM reunion WHERE id_reunion = '".$idR."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $image_doc = "./img_Events/";
        $ouverture = opendir ($image_doc); 
        $lecture = readdir($ouverture); 
        while (false !== ($fichier = readdir($ouverture))) 
        {
            $chemin = $image_doc."/".$fichier;
            //si le fichier n'est pas un répertoire
            if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier)) 
            {
                $nom = explode(".", $fichier);
                if($nom[0]==$idR)
                {
                    unlink($chemin); 
                } 
            }
        }
        $query="SELECT objet_reunion FROM reunion WHERE id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $Event=$prep->fetchAll(PDO::FETCH_ASSOC);
        foreach($participant as $v)
        {
            $query="INSERT INTO notification (contenu_notif, date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, NULL, :idP)";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->bindValue(':contenu', $_POST['message']);
            $prep->bindValue(':date', $_POST['currentDate']);
            $prep->bindValue(':statu', "0");
            $prep->bindValue(':type', "5");
            $prep->bindValue(':idP', $v["id_profil"]);
            $prep->execute();
            $query="SELECT token_profil FROM profil WHERE id_profil='".$v["id_profil"]."'";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $data=$prep->fetchAll(PDO::FETCH_ASSOC);          
            if(count($data)!=0)
            {
                notification($data[0]['token_profil'], "Réunion annulée", $_POST['message']);
            }
        };
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "Event supprimer";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }
            
        
            
    //************************************//
    //         fonctions comentaire       //
    //************************************//
    /** 
    *   Requête d'insertion de commentaire
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $contenu    : contenu du commentaire
    *       $date       : date du commentaire
    *       $heure      : heure du commentaire
    *       $idP        : id profil du commentateur
    *       $idR        : id de l'event
    *       $idC        : id du commentaire
    *   fonction:
    *   Insertion d'un commentaire dans la base de donnée avec
    *   tout ses invités.
    *   Attribution du commentaire a l'event associer.
    *   Insertion d'une notifications au profils concerné.
    *   Envoie d'une notification push au profils concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function commenter(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt); 
        $query="INSERT INTO commentaire (contenu_commentaire, date_commentaire, heure_commentaire, id_profil) VALUES(:commentaire, :date, :heure, :id)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':commentaire', $_POST['contenu_commentaire']);
        $prep->bindValue(':date', Date("Y-m-d", strtotime($_POST['date'])));
        $prep->bindValue(':heure', $_POST['heure']);
        $prep->bindValue(':id', $jwtData['sub']);
        $prep->execute();
        $idC=$pdo->lastInsertId();
        $query="INSERT INTO posseder (id_reunion, id_commentaire) VALUES(:idR, :idC)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':idR', $_POST['idR']);
        $prep->bindValue(':idC', $idC);
        $prep->execute();
        $query="SELECT id_profil FROM participant WHERE id_reunion='".$_POST['idR']."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $participant=$prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT P.id_profil FROM profil P INNER JOIN reunion R ON P.id_profil=R.id_profil WHERE R.id_reunion='".$_POST['idR']."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $data2=$prep->fetchAll(PDO::FETCH_ASSOC);
        array_push($participant,$data2[0]);
        if($_POST['notif']=="true")
        {
            foreach($participant as $v)
            {
                if($v["id_profil"]!=$jwtData['sub'])
                {
                    $query="INSERT INTO notification (contenu_notif, date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, :idR, :idP)";
                    $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
                    $prep->bindValue(':contenu',"Un participant a commenté une réunion");
                    $prep->bindValue(':date',$_POST['currentDate']);
                    $prep->bindValue(':statu',"0");
                    $prep->bindValue(':type', "3");
                    $prep->bindValue(':idR', $_POST['idR']);
                    $prep->bindValue(':idP', $v["id_profil"]);
                    $prep->execute(); 
                    $query="SELECT token_profil FROM profil WHERE id_profil='".$v["id_profil"]."'";
                    $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
                    $prep->execute();
                    $data=$prep->fetchAll(PDO::FETCH_ASSOC);             
                    if(count($data)!=0)
                    {
                        notification($data[0]['token_profil'], "commentaire", "Un participant a commenté un événement");
                    }
                }
            }            
        }
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "Event modifier";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]); 
    } 
            
        
            
    //************************************//
    //          fonctions Note            //
    //************************************//
    /** 
    *   Requête de création de note
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $note       : valeur de la note
    *       $idP        : id de l'utiliateur
    *       $idR        : id de l'events 
    *   fonction:
    *   Insertion d'une note dans la base de données
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function noter(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt); 
        $query="INSERT INTO note (valeur_note, id_profil, id_reunion) VALUES(:note, :idP, :idR)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':note', $_POST['note']);
        $prep->bindValue(':idP', $jwtData['sub']);
        $prep->bindValue(':idR', $_POST['idR']);
        $prep->execute();
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "Note envoyée";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]); 
    }
            
        
            
    /** 
    *   Requête de modification de note
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $note       : valeur de la note
    *       $idN        : id de la note 
    *   fonction:
    *   Insertion d'une note dans la base de données
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function updateNote(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);  
        $idN = $_POST['idN'];
        $note = $_POST['note'];
        $query="UPDATE note SET valeur_note = '".$note."' WHERE id_note = '".$idN."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "Note modifiée";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]); 
    }
            
        
            
    //************************************//
    // fonctions invitation/participation //
    //************************************//
    /** 
    *   Requête d'insertion d'invités
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $invite     : liste des profil a inseré
    *       $idR        : id de l'events 
    *   fonction:
    *   Insertion d'un invité dans la base de données
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function inviter(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $invite=json_decode($_POST['listInviter']); 
        $idR=$_POST['idR'];          
        foreach($invite as $v)
        {
            $query="INSERT INTO inviter (id_reunion, id_profil) VALUES(:idR, :idP)";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->bindValue(':idR', $idR);
            $prep->bindValue(':idP', $v);
            $prep->execute(); 
            $query="INSERT INTO notification (contenu_notif, date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, :idR, :idP)";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->bindValue(':contenu',"Vous êtes invité à la réunion '".$_POST['objet']."'");
            $prep->bindValue(':date', $_POST['currentDate']);
            $prep->bindValue(':statu', "0");
            $prep->bindValue(':type', "0");
            $prep->bindValue(':idR', $idR);
            $prep->bindValue(':idP', $v);
            $prep->execute(); 
            $query="SELECT token_profil FROM profil WHERE id_profil='".$v."'";
            $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
            $prep->execute();
            $data=$prep->fetchAll(PDO::FETCH_ASSOC);
            if(count($data)!=0)
            {
                notification($data[0]['token_profil'], "Invitation", "Vous avez reçu une invitation à '".$_POST['objet']."'");
            }   
        };
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "bien inviter";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }

        
            
    /** 
    *   Requête affiliation a un events
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idP        : id de l'utiliateur
    *       $idR        : id de l'events 
    *   fonction:
    *   Insertion d'un participanta l'event dans la base 
    *   de données.
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function rejoindre(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idP = $jwtData['sub'];
        $idR = $_POST['idR'];
        $query="INSERT INTO participant (id_profil, id_reunion) VALUES(:idP, :idR)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':idP', $jwtData['sub']);
        $prep->bindValue(':idR', $_POST['idR']);
        $prep->execute();
        $query="SELECT P.token_profil, P.id_profil FROM profil P INNER JOIN reunion R ON P.id_profil=R.id_profil WHERE R.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $data=$prep->fetchAll(PDO::FETCH_ASSOC);
        $query="INSERT INTO notification (contenu_notif,date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, :idR, :idP)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':contenu',"Un membre a rejoint votre réunion");
        $prep->bindValue(':date',$_POST['currentDate']);
        $prep->bindValue(':statu',"0");
        $prep->bindValue(':type', "1");
        $prep->bindValue(':idR', $idR);
        $prep->bindValue(':idP', $data[0]['id_profil']);
        $prep->execute(); 
        if(count($data)!=0)
        {
            notification($data[0]['token_profil'], "Nouveau participant", "Un membre a rejoint votre réunion");
        }
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "vous avez rejoint l'evénement";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]); 
    }
            
        
            
    /** 
    *   Requête de validation d'invitation
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idP        : id de l'utiliateur
    *       $idR        : id de l'events 
    *   fonction:
    *   Insertion d'un participant dans la base de données
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function accepter(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idP = $jwtData['sub'];
        $idR = $_POST['idR'];
        $query="DELETE FROM inviter WHERE  id_profil= '".$idP."' AND id_reunion = '".$idR."' ";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $query="INSERT INTO participant (id_profil, id_reunion) VALUES(:idP, :idR)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':idP', $jwtData['sub']);
        $prep->bindValue(':idR', $_POST['idR']);
        $prep->execute();
        $query="SELECT P.id_profil, P.token_profil FROM profil P INNER JOIN reunion R ON P.id_profil=R.id_profil WHERE R.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $data=$prep->fetchAll(PDO::FETCH_ASSOC);
        $query="INSERT INTO notification (contenu_notif,date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, :idR, :idP)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':contenu',"Un membre a accepté votre invitation ");
        $prep->bindValue(':date',$_POST['currentDate']);
        $prep->bindValue(':statu',"0");
        $prep->bindValue(':type', "6");
        $prep->bindValue(':idR', $idR);
        $prep->bindValue(':idP', $data[0]['id_profil']);
        $prep->execute(); 
        if(count($data)!=0)
        {
            notification($data[0]['token_profil'], "Invitation acceptée", "Un membre a accepté votre invitation");
        }
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "vous avez rejoint l'evénement";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }
            
        
            
    /** 
    *   Requête de refu d'invitation
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $note       : valeur de la note
    *       $idP        : id de l'utiliateur
    *       $idR        : id de l'events 
    *   fonction:
    *   supression de l'nvitation en base de données  
    *   Envoie d'une reponse de statut au client.
    */
    function refuser(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt); 
        $idP = $jwtData['sub'];
        $idR = $_POST['idR'];
        $query="DELETE FROM inviter WHERE  id_profil= '".$idP."' AND id_reunion = '".$idR."' ";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "Invitation refusée";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }
            
        
    /** 
    *   Requête de desistement
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idP        : id de l'utiliateur
    *       $idR        : id de l'events 
    *   fonction:
    *   Supression d'un participant dans la base de données
    *   Insertion d'une notifications au profil concerné.
    *   Envoie d'une notification push au profil concerné.  
    *   Envoie d'une reponse de statut au client.
    */
    function partir(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idP = $_POST['idProfil'];
        $idR = $_POST['idReunion'];
        $query="DELETE FROM participant WHERE  id_profil= '".$idP."' AND id_reunion = '".$idR."' ";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $query="SELECT P.token_profil, P.id_profil FROM profil P INNER JOIN reunion R ON P.id_profil=R.id_profil WHERE R.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $data=$prep->fetchAll(PDO::FETCH_ASSOC);
                
        $query="INSERT INTO notification (contenu_notif, date_notif, statu_notif, type_notif, id_reunion, id_profil) VALUES(:contenu, :date, :statu, :type, :idR, :idP)";
        $prep=$pdo->prepare($query)or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->bindValue(':contenu', "Un participant a quitté votre réunion");
        $prep->bindValue(':date', $_POST['currentDate']);
        $prep->bindValue(':statu', "0");
        $prep->bindValue(':type', "4");
        $prep->bindValue(':idR', $idR);
        $prep->bindValue(':idP', $data[0]['id_profil']);
        $prep->execute();           
        if(count($data)!=0)
        {
            notification($data[0]['token_profil'], "Un participant est parti", "Un participant a quitté votre réunion");
        }
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "Event quitter";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }
            
        
            
    //************************************//
    //      fonctions notification        //
    //************************************//
    /** 
     *   Requête de selection des notifications
     *   
     *   Methode :    POST
     *   params  : 
     *       $action     : id de l'action a effectuer
     *       $idP        : id de l'utiliateur
     *   fonction:
     *   Recuperation des notification en base de données 
     *   Envoie d'une reponse de statut au client.
     */
    function AfficheNotification(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);  
        $idP=$jwtData['sub'];
        $query="SELECT * FROM notification WHERE id_profil='".$idP."' ORDER BY date_notif DESC";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $data['notif']=$prep->fetchAll(PDO::FETCH_ASSOC);
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($data+['jwt'=>$jwt]);
    }
                
        
    /** 
    *   Requête de supression des notifications
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idN        : id de la notification
    *   fonction:
    *   supression des notification en base de données 
    *   Envoie d'une reponse de statut au client.
    */
    function suppNotification(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idN=$_POST['idN'];
        $query="DELETE FROM notification WHERE id_notif='".$idN."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "notification supprimer";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }    
    
                
        

    /** 
    *   Requête d'update des notifications
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idN        : id de la notification
    *   fonction:
    *   modification du statut des notification en base de données 
    *   Envoie d'une reponse de statut au client.
    */
    function checkNotification(){    
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);    
        $idN=$_POST['idN'];
        $query="UPDATE notification SET statu_notif = '1' WHERE id_notif = '".$idN."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "notification checkée";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]);
    }    
    
                
    
    /** 
    *   Requête de supression des notifications
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idN        : id de la notification
    *   fonction:
    *   Supression des notification cheked en base de données 
    *   Envoie d'une reponse de statut au client.
    */
    function suppCheckedNotification(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idP=$jwtData['sub'];
        $query="DELETE FROM notification WHERE id_profil='".$idP."' AND statu_notif = 1";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "notification supprimer";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]); 
    }
                
        
    /** 
    *   Requête de supression des notifications
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idN        : id de la notification
    *   fonction:
    *   Supression de toute les notifications en base de données 
    *   Envoie d'une reponse de statut au client.
    */
    function suppAllNotification(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idP=$jwtData['sub'];
                    
        $query="DELETE FROM notification WHERE id_profil='".$idP."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $var = array();
        $var["res"]  = "True";
        $var["msg"]  = "notification supprimé";
        $jwt = jwtEncodeFromPayload($jwtData);
        echo json_encode($var+['jwt'=>$jwt]); 
    } 
        


    //************************************//
    //          fonctions Site            //
    //************************************//
    /** 
    *   Requête de selection des site
    *   
    *   Methode :    POST
    *   params  : 
    *       $action     : id de l'action a effectuer
    *       $idN        : id de la notification
    *   fonction:
    *   Récuperation des notification cheked en base de données 
    *   Envoie d'une reponse de statut au client.
    */
    function allSite(){
        $pdo = init_pdo();
        $query="SELECT * FROM site";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $data=$prep->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

//************************************//
//   fonction réinitialisation mdp    //
//************************************//
/** 
*   Requête de réinitialisation du mot de passe
*   
*   Methode :    POST
*   params  : 
*       $mail     : mail de l'utilisateur
*       $reponse     : reponse à la question secrète
*       $newPassword     : nouveau mot de passe
*   fonction:
*   Permet de réinitialiser le mot de passe de l'utilisateur par un nouveau si la réponse fourni correspond bien
*   Envoie d'une reponse de statut au client.
*/
                     
function ResetPassword(){
    $pdo = init_pdo();
    $mail=$_POST['mail'];
    $newPassword=$_POST['newPassword'];
    $reponse=$_POST['reponse'];
    
    if(isset($mail)&&isset($newPassword)&&isset($reponse))
    {
        if(!empty($mail) && !empty($newPassword) && !empty($reponse))
        {
            $query="SELECT reponse_profil FROM profil WHERE mail_profil = '".$mail."'";
            $prep=$pdo->prepare($query);
            $prep->execute();
            $data=$prep->fetchAll(PDO::FETCH_ASSOC);
            $var = array();
            if(Count($data)==0)
            {
                $var["res"] = "False";
                $var["msg"]  = "Requete SQL vide";
                echo json_encode($var);
            }
            else
            {
                if(password_verify($reponse, $data[0]['reponse_profil']) == false )
                {
                    $var["res"] = "False";
                    $var["msg"] = "Mauvaise reponse";
                    echo json_encode($var);
                }    
                else
                {
                    $var["res"]  = "True";
                    $var["msg"]  = "Mot de passe reinitialise";
                    $hashedPassword = password_hash($_POST['newPassword'], PASSWORD_DEFAULT);
                    $query="UPDATE profil SET password_profil='".$hashedPassword."' WHERE mail_profil='".$mail."'";
                    $prep=$pdo->prepare($query);
                    $prep->execute();       
                    echo json_encode($var);
                } 
            }
        }
    }
}
     

    //************************************//
    //         fonction note            //
    //************************************//
    /** 
    *   Requête de selection des site
    *   
    *   Methode :    Get
    *   params  :    idP ID du profile de l'organisateur

    */


    function noteUser(){

        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idP=$jwtData['sub'];

        $query="SELECT AVG(valeur_note) FROM note INNER JOIN reunion ON note.id_reunion = reunion.id_reunion WHERE reunion.id_profil='".$idP."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !');
        $prep->execute();
        $data=$prep->fetchAll(PDO::FETCH_ASSOC);
        if(is_null($data[0]["AVG(valeur_note)"])){
            $data[0]["AVG(valeur_note)"]=0;
        }
        echo json_encode(["AVG"=>$data[0]["AVG(valeur_note)"]]+['jwt'=>$jwt]); 

    }

//************************************//
//   fonction de récupération status  //
//************************************//
/** 
*   Requête de récupération status
*   
*   Methode :    POST
*   params  : 
*       $jwt    : token jwt
*       $idR    : ID de la réunion dont nou voulons étudier le status du profil
*   fonction:
*   Permet d'évaluer le status d'un profil au sein d'unre réunion'
*/
    function getStatusProfil(){
        $pdo = init_pdo();
        $jwt = $_POST['jwt'];
        $jwtData = jwtDecode($jwt);
        $idP=$jwtData['sub'];
        $idR= intval($_POST['idR']);
        $element=array();
        $query="SELECT P.id_profil FROM profil P INNER JOIN reunion R ON P.id_profil=R.id_profil WHERE id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["organisateur"] = $prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT P.id_profil FROM profil P INNER JOIN participant PA ON P.id_profil=PA.id_profil INNER JOIN reunion R ON R.id_reunion=PA.id_reunion INNER JOIN site S ON P.id_site=S.id_site WHERE R.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["participants"] = $prep->fetchAll(PDO::FETCH_ASSOC);
        $query="SELECT P.id_profil FROM profil P INNER JOIN inviter I ON P.id_profil=I.id_profil INNER JOIN reunion R ON R.id_reunion=I.id_reunion INNER JOIN site S ON P.id_site=S.id_site  WHERE R.id_reunion='".$idR."'";
        $prep=$pdo->prepare($query) or die('Erreur SQL !<br>'.$pdo->errorInfo());
        $prep->execute();
        $element["inviter"] = $prep->fetchAll(PDO::FETCH_ASSOC);

        foreach ($element["inviter"] as $invite) {
            if ($idP == $invite["id_profil"]) // s'il est inviter
            {
                echo json_encode(['jwt'=>$jwt,'status'=>2]); 
                return;
            }
        }

        if ($idP == $element["organisateur"][0]["id_profil"]) // s'il est organisateur
        {
            echo json_encode(['jwt'=>$jwt,'status'=>3]); 
                return;
        }
        foreach ($element["participants"] as $participant) {
            if ($idP == $participant["id_profil"]) // s'il est inviter
            {
                echo json_encode(['jwt'=>$jwt,'status'=>1]); 
                return;
            }
        }
        echo json_encode(['jwt'=>$jwt,'status'=>0]); 
        return;// s'il est */
    }

    if(isset($_GET['action'])&& !preg_grep ('/^'.$_GET['action'].'/i', $lockFunctions))
    {
        $action = $_GET['action'];
        
        try{
            call_user_func($action);
            if(isset($_POST["jwt"])){
                $jwt = $_POST['jwt'];
                $jwtData = jwtDecode($jwt);
                $id = $jwtData['sub'];
                trace($action,$_GET+$_POST,$id);
            }else{
                trace($action,$_GET+$_POST);
            }
        }catch(Exception $e){
            
            // set response code
            http_response_code(404);
            
            // show error message
            echo json_encode(array(
                "message" => "Url not found.",
                "error" => $e->getMessage()
            ));
            die();
        }
        
    }else{
        // set response code
        http_response_code(404);
        
        // show error message
        echo json_encode(array(
            "message" => "Url not found.",
            "error" => $e->getMessage()
        ));
        die();
    }
                    
?>